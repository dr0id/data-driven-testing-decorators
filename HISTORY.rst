.. :changelog:

History
-------

2.0.3.0 (2015-06-10)
++++++++++++++++++++
    Renamed following to avoid conflicts with testing frameworks finding the actual tests, e.g. nose looks for anything
    that is callable and satisfies the regex "(?:^|[\b_\.\-])[Tt]est" which caused nose to run 'test_case' and
    'use_data_driven_testing_decorators' as a unittest causing an error.
       * test_case -> data_driven
       * test_case_from_generator -> gnerator_driven
       * use_data_driven_testing_decorators -> use_data_driven_decorators

1.0.0.0 (2014-06-13)
++++++++++++++++++++

* First release on Bitbucket.
