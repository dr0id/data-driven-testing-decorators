============
Installation
============

At the command line::

    $ easy_install datadriventestingdecorators

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv datadriventestingdecorators
    $ pip install datadriventestingdecorators
