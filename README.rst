===========================
datadriventestingdecorators
===========================

.. image:: https://badge.fury.io/py/data-driven-testing-decorators.png
    :target: http://badge.fury.io/py/data-driven-testing-decorators
    
.. image:: https://pypip.in/d/data-driven-testing-decorators/badge.png
        :target: https://crate.io/packages/data-driven-testing-decorators?version=latest


Data driven testing decorators.

* Free software: BSD license
* Documentation: http://data-driven-testing-decorators.rtfd.org.
* Discussion: https://dr0id.bitbucket.org/python/datadriventestingdecorators


Version 2.0.3.0 -- What happend!?
=================================

After much debugging and fiddling around, I found out why my unittests for the decorators where failing with nose:
it was an obvious reason once the cause was found. The problem was, that nose treats any methods that match following
regex "(?:^|[\b_\.\-])[Tt]est" as a test method to run independently if the class inherited from unittest.TestCase or
not. This was causing an error. The solution therefore is to rename the decorators to not contains anything that
could match the regex anymore.

Features
--------

Data driven tests through decorators. Inspired by https://github.com/txels/ddt and Nunit TestCase style
( http://www.nunit.org/index.php?p=testCase&r=2.6.2 ).


* powerfull test case decorator
* generator function decorator to allow reading from a file or otherwise generated test case data
* find failing test case through name of failing test method


Examples:

.. sourcecode:: python

        @ddt.use_data_driven_decorators
        class ArithmeticTests(unittest.TestCase):
            """
            These class demonstrates the usage of the data_driven decorator.
            It will be used in different ways on the methods. For the details see
            the comments behind the decorators.
            """

            @ddt.data_driven(1, 4, 4)
            @ddt.data_driven(2, 6, 3)
            @ddt.data_driven(3, 6, 2)
            @ddt.data_driven(3, 6, 0, expected_exception=ZeroDivisionError) # expecting the ZeroDivisionError
            def test_division(self, expected, numerator, denominator):
                """
                Simple test method that verifies the division. This test method gets the expected value as an argument.
                :param expected: expected result
                :param numerator: the numerator of the division
                :param denominator: the denominator of the division
                """
                # arrange
                # act
                actual = numerator / denominator
                # verify
                self.assertEqual(expected, actual)

            @ddt.data_driven(1, 2, result=5)  # fail because result is different
            @ddt.data_driven(1, 2, result=3)
            @ddt.data_driven(1, 3)  # error because result=x is missing but method has a return value
            def test_add(self, a, b):
                """
                Simple test method that tests the addition of two numbers. Note that the method returns the calculated
                value. To test this value note the 'result=...' argument in the data_driven decorator. Note also, that two out
                of the three test_cases do fail.
                :param a: first argument for the addition
                :param b: second argument for the addition
                :return: the added value
                """
                return a + b

            @ddt.generator_driven(generate_values)
            def test_division(self, expected_result, dividend, divisor):
                """
                This is the test method which will be tested against all values of the generator.
                :param expected_result: the expected result
                :param dividend: the dividend
                :param divisor: the divisor
                """
                self.assertEqual(expected_result, dividend / divisor)

            def generate_values():
                for i in range(100):
                    yield (i, i * i, i)


I hope this helps for the first steps. Happy testing!
