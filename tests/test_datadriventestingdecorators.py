# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of HG_pyweek_games
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Unittests to verify functionality.
"""
from __future__ import print_function

__version__ = '2.0.3.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = 'DR0ID'
__email__ = 'dr0iddr0id [at] googlemail [dot] com'
__copyright__ = "DR0ID @ 2014"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

import unittest
import re

import datadriventestingdecorators as mut  # module under test
from datadriventestingdecorators import data_driven, use_data_driven_decorators, MatchType, EXPECTED_EXCEPTION, \
    TEST_NAME


class TestVersions(unittest.TestCase):
    """
    This class test the version of this test module to be equal to the version of the module that is tested.
    """

    def test_version_should_be_equal_to_mut_version(self):
        """
        Tests if the module versions are equal.
        """
        # arrange / act / verify
        self.assertTrue(mut.__version_info__ == __version_info__,
                        "version of module under test should match version of this tests !")


class MethodDecoratorDataPointsTestCaseTests(unittest.TestCase):
    def test_decorator_test_case_returns_wrapped_function_if_only_result_is_provided(self):
        # arrange
        # act
        wrapped_func = mut.data_driven(1, [1], None, "")

        # verify
        self.assertNotEqual(wrapped_func, None, "wrapped function should not be None")
        self.assertTrue(wrapped_func != mut.data_driven)

    def test_decorator_test_case_returns_wrapped_function_if_only_exception_is_provided(self):
        # arrange
        # act
        wrapped_func = mut.data_driven(1, 3, 0, expected_exception=ZeroDivisionError)

        # verify
        self.assertNotEqual(wrapped_func, None, "wrapped function should not be None")
        self.assertTrue(wrapped_func != mut.data_driven)

    def test_decorator_test_case_wrapped_function_adds_attribute_on_function(self):
        # arrange
        # act
        wrapped_func = mut.data_driven(1, [1], None, "")
        result = wrapped_func(func_mock)

        # verify
        # noinspection PyProtectedMember
        self.assertTrue(hasattr(result, mut._DATA_TEST_CASE_ATTRIBUTE))

        # cleanup
        # noinspection PyProtectedMember
        delattr(func_mock, mut._DATA_TEST_CASE_ATTRIBUTE)

    def test_decorator_test_case_wrapped_function_returns_same_function(self):
        # arrange
        # act
        wrapped_func = mut.data_driven(1, [1], None, "")
        result = wrapped_func(func_mock)

        # verify
        self.assertEqual(result, func_mock)

        # cleanup
        # noinspection PyProtectedMember
        delattr(func_mock, mut._DATA_TEST_CASE_ATTRIBUTE)

    def test_decorator_test_case_wrapped_function_attribute_is_not_empty(self):
        # arrange
        # act
        wrapped_func = mut.data_driven(1, [1], None, "")
        result = wrapped_func(func_mock)

        # verify
        # noinspection PyProtectedMember
        attr = getattr(result, mut._DATA_TEST_CASE_ATTRIBUTE)
        self.assertTrue(len(attr) > 0)

        # cleanup
        # noinspection PyProtectedMember
        delattr(func_mock, mut._DATA_TEST_CASE_ATTRIBUTE)

    def test_decorator_test_case_wrapped_function_attribute_is_extended_if_already_exists(self):
        # arrange
        mut.data_driven(1, [1], None, "")(func_mock)

        # act
        wrapped_func = mut.data_driven(2, [2], None, "")
        result = wrapped_func(func_mock)

        # verify
        # noinspection PyProtectedMember
        attr = getattr(result, mut._DATA_TEST_CASE_ATTRIBUTE)
        self.assertEqual(len(attr), 2)

        # cleanup
        # noinspection PyProtectedMember
        delattr(func_mock, mut._DATA_TEST_CASE_ATTRIBUTE)

    def test_decorator_test_case_throws_if_no_args_are_provided(self):
        # arrange
        # act
        # verify
        wrapped = mut.data_driven()
        self.assertRaises(ValueError, wrapped)

    def test_decorator_test_case_throws_if_empty_args_are_provided(self):
        # arrange
        # act
        # verify
        wrapped = mut.data_driven(args=[])
        self.assertRaises(ValueError, wrapped)

    def test_decorator_test_cases_method_using_kwargs(self):
        # arrange
        @use_data_driven_decorators
        class InternalTest(object):
            """
            The class.
            """

            @data_driven(1, 2, function_kwargs={'3': 3, '4': 4}, result=10, test_name="AA")
            def test_bla(self, a, *args, **kwargs):
                """
                Testmethod.
                :param a: first argument
                :param args: the argument list
                :param kwargs: the kwargs
                :return: the calculated value
                """
                b = args[0]
                return a + b + kwargs['3'] + kwargs['4']

        mock = InternalTest()

        # act
        mock.test_bla_DDT_AA()

        # verify
        # verified through data_driven decorator

    def test_decorator_test_case_wrong_return_value_throws_assert(self):
        # arrange
        @use_data_driven_decorators
        class InternalTest(object):
            """
            The internal test class.
            """

            @data_driven(1, 1, result=3, test_name="AA")
            def test_bla(self, a, b):
                """
                The test method.
                :param a: first argument
                :param b: second argument
                :return: the calculated value (the addition of the two arguments)
                """
                return a + b

        mock = InternalTest()

        # act
        # verify
        self.assertRaises(AssertionError, mock.test_bla_DDT_AA)

    def test_decorator_test_case_assert_return_value(self):
        # arrange

        @use_data_driven_decorators
        class InternalTest(object):
            """
            The internal test class.
            """

            @data_driven(1, 1, result=2, test_name="AA")
            def test_bla(self, a, b):
                """
                The test method.
                :param a: first argument
                :param b: second argument
                :return: the calculated value (the addition of the two arguments)
                """
                return a + b

        mock = InternalTest()

        # act
        # verify
        mock.test_bla_DDT_AA()

    def test_decorator_test_case_warns_about_return_value_if_no_result_in_kwargs(self):
        # arrange

        @use_data_driven_decorators
        class InternalTest(object):
            """
            The internal test class.
            """

            @data_driven(1, 1, test_name="AA")
            def test_bla(self, a, b):
                """
                The test method.
                :param a: first argument
                :param b: second argument
                :return: the calculated value (the addition of the two arguments)
                """
                return a + b

        mock = InternalTest()

        # act
        # verify
        self.assertRaises(UserWarning, mock.test_bla_DDT_AA)


class ClassDecoratorTestsForTestCase(unittest.TestCase):
    def test_decorator_test_case_arguments_are_passed_to_method(self):
        # arrange
        arguments = (1, 1, 2, 3)

        @use_data_driven_decorators
        class MockTestCase(object):
            """
            The internal test class used as a mock.
            """

            def __init__(self):
                self.passed_args = []

            @data_driven(*arguments, test_name="MyFantasticName")
            def mocked_function(self, *args):
                """
                The test method of the mock class.
                :param args: the arguments to register.
                """
                self.passed_args = args

        mock = MockTestCase()

        # act
        mock.mocked_function_DDT_MyFantasticName()

        # verify
        self.assertEqual(mock.passed_args, arguments)

    def test_decorator_test_case_description_kwarg_is_set_as_doc_string(self):
        # arrange
        arguments = (1, 1, 2, 3)
        description = "Description which is set as documentation string."

        @use_data_driven_decorators
        class MockTestCase(object):
            """
            The internal test class used as a mock.
            """

            def __init__(self):
                self.passed_args = []

            @data_driven(*arguments, test_name="MyFantasticName", description=description)
            def mocked_function(self, *args):
                """
                The test method of the mock class.
                :param args: the arguments to register.
                """
                self.passed_args = args

        mock = MockTestCase()

        # act
        mock.mocked_function_DDT_MyFantasticName()

        # verify
        doc = mock.mocked_function_DDT_MyFantasticName.__doc__
        self.assertEqual(doc, description)

    def test_decorator_test_case_too_many_arguments_are_passed_to_method(self):
        # arrange
        arguments = (1, 2, 3, 4, 5, 6)

        @mut.use_data_driven_decorators
        class MockTestCase(object):
            """
            The internal test class used as a mock.
            """

            def __init__(self):
                self.passed_args = []

            @mut.data_driven(*arguments, test_name="MyFantasticName")
            def mocked_function(self, a, b, c):
                """
                The test method of the mock class.
                :param a: first argument
                :param b: second argument
                :param c: third argument
                """
                pass  # pragma: no cover

        mock = MockTestCase()

        # act
        # verify
        self.assertRaises(TypeError, mock.mocked_function_DDT_MyFantasticName)

    def test_decorator_test_case_is_asserted_for_expected_exception(self):
        # arrange
        arguments = (1, 2, 3)

        @use_data_driven_decorators
        class MockTestCase(unittest.TestCase):
            def __init__(self, method_name='runTest'):
                super(MockTestCase, self).__init__(methodName=method_name)
                self.passed_args = []

            @data_driven(*arguments, test_name="MyFantasticName", expected_exception=ValueError)
            def mocked_function(self, a, b, c):
                self.passed_args = (a, b, c)
                raise ValueError("my message")

        mock = MockTestCase(method_name='mocked_function_DDT_MyFantasticName')

        # act
        # verify
        mock.mocked_function_DDT_MyFantasticName()

    def test_decorator_test_case_is_asserted_fails_for_wrong_exception(self):
        # arrange
        arguments = (1, 2, 3)

        @use_data_driven_decorators
        class MockTestCase(object):
            def __init__(self):
                self.passed_args = []

            @data_driven(*arguments, test_name="MyFantasticName", expected_exception=ValueError)
            def mocked_function(self, a, b, c):
                self.passed_args = (a, b, c)
                raise TypeError()

        mock = MockTestCase()

        # act
        # verify
        self.assertRaises(AssertionError, mock.mocked_function_DDT_MyFantasticName)

    def test_decorator_test_case_with_same_arguments_already_exists_exception(self):
        # arrange
        arguments = (1, 2, 3)

        # act
        # verify
        try:
            # noinspection PyUnusedLocal
            @use_data_driven_decorators
            class MockTestCase(unittest.TestCase):
                # def __init__(self):
                #     super(MockTestCase, self).__init__()
                #     self.passed_args = []

                @data_driven(*arguments, test_name="MyFantasticName", expected_exception=ValueError)
                @data_driven(*arguments, test_name="MyFantasticName", expected_exception=ValueError)
                def mocked_function(self, a, b, c):
                    pass  # pragma: no cover
        except AttributeError:
            return  # pass, expected
        self.fail("should have raised AttributeError")  # pragma: no cover

    def test_decorator_test_case_with_same_arguments_but_different_kwargs_does_not_fail(self):
        # arrange
        arguments = (1, 2, 3)

        # act
        # verify
        try:
            # noinspection PyUnusedLocal
            @use_data_driven_decorators
            class MockTestCase(unittest.TestCase):
                # def __init__(self):
                #     super(MockTestCase, self).__init__()
                #     self.passed_args = []

                @data_driven(*arguments, expected_exception=TypeError)
                @data_driven(*arguments, expected_exception=ValueError)
                def mocked_function(self, a, b, c):
                    pass  # pragma: no cover
        except Exception as ex:  # pragma: no cover
            self.fail(ex)  # pragma: no cover

    def test_decorator_test_case_raises_exception_for_unknown_kwarg(self):
        # arrange
        arguments = (1, 2, 3)

        try:
            # act
            # noinspection PyUnusedLocal
            @use_data_driven_decorators
            class MockTestCase(unittest.TestCase):
                # def __init__(self):
                #     super(MockTestCase, self).__init__()
                #     self.passed_args = []

                @data_driven(*arguments, test_name="MyFantasticName", expected_exception=ValueError, unknown_kwarg=1)
                def mocked_function(self, a, b, c):
                    raise TypeError()  # pragma: no cover

        # verify
        except AttributeError:
            return  # expected
        self.fail("should have raised AttributeError for unknown kwarg")  # pragma: no cover

    def test_decorator_test_case_allow_unknown_kwarg(self):
        # arrange
        arguments = (1, 2, 3)

        try:
            # act
            # noinspection PyUnusedLocal
            @use_data_driven_decorators
            class MockTestCase(unittest.TestCase):
                # def __init__(self):
                #     super(MockTestCase, self).__init__()
                #     self.passed_args = []

                @data_driven(*arguments, test_name="MyFantasticName", expected_exception=ValueError, unknown_kwarg=1,
                           strict=False)
                def mocked_function(self, a, b, c):
                    raise TypeError()  # pragma: no cover

        # verify
        except AttributeError:  # pragma: no cover
            self.fail("should not have raised AttributeError for unknown kwarg")  # pragma: no cover

    def test_decorator_test_case_raises_for_unknown_kwarg(self):
        # arrange
        arguments = (1, 2, 3)

        try:
            # act
            # noinspection PyUnusedLocal
            @use_data_driven_decorators
            class MockTestCase(unittest.TestCase):

                @data_driven(*arguments, test_name="MyFantasticName", expected_exception=ValueError, unknown_kwarg=1,
                           strict=True)
                def mocked_function(self, a, b, c):
                    pass  # pragma: no cover

        # verify
        except AttributeError:
            return
        self.fail("should have raised AttributeError for unknown kwarg")  # pragma: no cover

    def test_decorator_test_case_check_expected_message_exact(self):
        # arrange
        arguments = (1, 2, 3)
        msg = "An expected message"

        @use_data_driven_decorators
        class MockTestCase(object):
            @data_driven(*arguments, test_name="MyFantasticName", expected_exception=TypeError, expected_message=msg)
            def mocked_function(self, a, b, c):
                raise TypeError(msg)

        mock = MockTestCase()
        # act
        # verify
        mock.mocked_function_DDT_MyFantasticName()

    def test_decorator_test_case_check_expected_message_exact_asserts_wrong_message(self):
        # arrange
        arguments = (1, 2, 3)
        msg = "An expected message"

        @use_data_driven_decorators
        class MockTestCase(object):
            @data_driven(*arguments, test_name="MyFantasticName", expected_exception=TypeError, expected_message=msg)
            def mocked_function(self, a, b, c):
                raise TypeError("this is the wrong message")

        mock = MockTestCase()

        # act
        # verify
        self.assertRaises(AssertionError, mock.mocked_function_DDT_MyFantasticName)

    def test_decorator_test_case_check_expected_message_contains(self):
        # arrange
        arguments = (1, 2, 3)
        msg = "An expected message xyz"

        @use_data_driven_decorators
        class MockTestCase(object):
            @data_driven(*arguments, test_name="MyFantasticName", expected_exception=TypeError, expected_message="xyz",
                       match_type=MatchType.CONTAINS)
            def mocked_function(self, a, b, c):
                raise TypeError(msg)

        mock = MockTestCase()

        # act
        # verify
        mock.mocked_function_DDT_MyFantasticName()

    def test_decorator_test_case_check_expected_message_contains_asserts_wrong_message(self):
        # arrange
        arguments = (1, 2, 3)

        @use_data_driven_decorators
        class MockTestCase(object):
            @data_driven(*arguments, test_name="MyFantasticName", expected_exception=TypeError, expected_message="xyz",
                       match_type=MatchType.CONTAINS)
            def mocked_function(self, a, b, c):
                raise TypeError("this is the wrong message")

        mock = MockTestCase()

        # act
        # verify
        self.assertRaises(AssertionError, mock.mocked_function_DDT_MyFantasticName)

    def test_decorator_test_case_check_expected_message_startswith(self):
        # arrange
        arguments = (1, 2, 3)
        msg = "xyz An expected message"

        @use_data_driven_decorators
        class MockTestCase(object):
            @data_driven(*arguments, test_name="MyFantasticName", expected_exception=TypeError, expected_message="xyz",
                       match_type=MatchType.STARTS_WITH)
            def mocked_function(self, a, b, c):
                raise TypeError(msg)

        mock = MockTestCase()
        # act
        # verify
        mock.mocked_function_DDT_MyFantasticName()

    def test_decorator_test_case_check_expected_message_startswith_asserts_wrong_message(self):
        # arrange
        arguments = (1, 2, 3)
        msg = "abc An expected message abc"

        @use_data_driven_decorators
        class MockTestCase(object):
            @data_driven(*arguments, test_name="MyFantasticName", expected_exception=TypeError, expected_message="xyz",
                       match_type=MatchType.STARTS_WITH)
            def mocked_function(self, a, b, c):
                raise TypeError(msg)

        mock = MockTestCase()

        # act
        # verify
        self.assertRaises(AssertionError, mock.mocked_function_DDT_MyFantasticName)

    def test_decorator_test_case_check_expected_message_regex(self):
        # arrange
        arguments = (1, 2, 3)
        msg = "orchestra"

        @use_data_driven_decorators
        class MockTestCase(object):
            @data_driven(*arguments, test_name="MyFantasticName", expected_exception=TypeError, expected_message="c",
                       match_type=MatchType.REGEX)
            def mocked_function(self, a, b, c):
                raise TypeError(msg)

        mock = MockTestCase()

        # act
        # verify
        mock.mocked_function_DDT_MyFantasticName()

    def test_decorator_test_case_check_expected_message_regex_asserts_wrong_message(self):
        # arrange
        arguments = (1, 2, 3)
        msg = "message"

        @use_data_driven_decorators
        class MockTestCase(object):
            @data_driven(*arguments, test_name="MyFantasticName", expected_exception=TypeError, expected_message="c",
                       match_type=MatchType.REGEX)
            def mocked_function(self, a, b, c):
                raise TypeError(msg)

        mock = MockTestCase()

        # act
        # verify
        self.assertRaises(AssertionError, mock.mocked_function_DDT_MyFantasticName)

    def test_decorator_test_case_check_expected_message_regex_with_flags(self):
        # arrange
        arguments = (1, 2, 3)
        msg = "orchestra"

        @use_data_driven_decorators
        class MockTestCase(object):
            @data_driven(*arguments, test_name="MyFantasticName", expected_exception=TypeError,
                       expected_message=("C", re.I),
                       match_type=MatchType.REGEX)
            def mocked_function(self, a, b, c):
                raise TypeError(msg)

        mock = MockTestCase()

        # act
        # verify
        mock.mocked_function_DDT_MyFantasticName()

    def test_decorator_test_case_check_expected_message_regex_with_flags_asserts_wrong_message(self):
        # arrange
        arguments = (1, 2, 3)
        msg = "message"

        @use_data_driven_decorators
        class MockTestCase(object):
            @data_driven(*arguments, test_name="MyFantasticName", expected_exception=TypeError,
                       expected_message=("z", re.I),
                       match_type=MatchType.REGEX)
            def mocked_function(self, a, b, c):
                raise TypeError(msg)

        mock = MockTestCase()

        # act
        # verify
        self.assertRaises(AssertionError, mock.mocked_function_DDT_MyFantasticName)

    def test_decorator_test_case_check_expected_message_regex_with_too_many_arguments(self):
        # arrange
        arguments = (1, 2, 3)
        msg = "message"

        @use_data_driven_decorators
        class MockTestCase(object):
            @data_driven(*arguments, test_name="MyFantasticName", expected_exception=TypeError,
                       expected_message=("z", re.I, "too much"),
                       match_type=MatchType.REGEX)
            def mocked_function(self, a, b, c):
                raise TypeError(msg)

        mock = MockTestCase()

        # act
        # verify
        self.assertRaises(AssertionError, mock.mocked_function_DDT_MyFantasticName)

    def test_decorator_test_case_unknown_match_type(self):
        # arrange
        arguments = (1, 2, 3)
        msg = "message"

        @use_data_driven_decorators
        class MockTestCase(object):
            @data_driven(*arguments, test_name="MyFantasticName", expected_exception=TypeError,
                       expected_message=("z", re.I),
                       match_type=MatchType.REGEX + 100)
            def mocked_function(self, a, b, c):
                raise TypeError(msg)

        mock = MockTestCase()

        # act
        # verify
        self.assertRaises(TypeError, mock.mocked_function_DDT_MyFantasticName)


# noinspection PyUnusedLocal
def generator_function(*args, **kwargs):
    """
    A generator function mock. Used in the unittests to verify certain things.
    :param args: the argument list
    :param kwargs: the named arguments dictionary
    """
    pass  # pragma: no cover


def func_mock():
    """
    A function mock. Used in the unittests.
    """
    pass  # pragma: no cover


class GeneratorDecoratorTestCasesTests(unittest.TestCase):
    def test_returned_wrapped_function_is_not_none(self):
        # arrange
        # act
        wrapped_func = mut.generator_driven(generator_function)
        # verify
        self.assertNotEqual(wrapped_func, None, "wrapped function should not be None")

    def test_returned_wrapped_function_is_not_generator_function(self):
        # arrange
        # act
        wrapped_func = mut.generator_driven(generator_function)
        # verify
        self.assertNotEqual(wrapped_func, mut.generator_driven)

    def test_throws_exception_if_no_generator_is_provided(self):
        # arrange
        # act
        # verify
        wrapped = mut.generator_driven(None)
        self.assertRaises(ValueError, wrapped, None)

    def test_adds_attribute(self):
        # arrange

        # act
        wrapper = mut.generator_driven(generator_function)
        actual = wrapper(func_mock)

        # verify
        # noinspection PyProtectedMember
        self.assertTrue(hasattr(actual, mut._GENERATOR_TEST_CASE_ATTRIBUTE))

        # cleanup
        # noinspection PyProtectedMember
        delattr(func_mock, mut._GENERATOR_TEST_CASE_ATTRIBUTE)

    def test_wrapped_function_returns_same_function(self):
        # arrange

        # act
        wrapper = mut.generator_driven(generator_function)
        actual = wrapper(func_mock)

        # verify
        self.assertTrue(actual == func_mock)

        # cleanup
        # noinspection PyProtectedMember
        delattr(func_mock, mut._GENERATOR_TEST_CASE_ATTRIBUTE)

    def test_added_attribute_is_not_empty(self):
        # arrange
        args = (1, 2, 3, 4, 5, 6)
        kwargs = {'1': 1, '2': 2, '3': 3}

        # act
        wrapper = mut.generator_driven(generator_function, *args, **kwargs)
        actual = wrapper(func_mock)

        # verify
        # noinspection PyProtectedMember
        actual_generator, actual_args, actual_kwargs = getattr(actual, mut._GENERATOR_TEST_CASE_ATTRIBUTE)[0]

        self.assertTrue(actual_generator == generator_function)
        self.assertEqual(actual_args, args)
        self.assertEqual(actual_kwargs, kwargs)

        # cleanup
        # noinspection PyProtectedMember
        delattr(func_mock, mut._GENERATOR_TEST_CASE_ATTRIBUTE)

    def test_multiple_generator_test_cases_are_allowed_on_method(self):
        # arrange
        args1 = (1, 2)
        args2 = (3, 4)
        kwargs1 = {'1': 1}
        kwargs2 = {'2': 2}

        # act
        wrapper1 = mut.generator_driven(generator_function, *args1, **kwargs1)
        wrapper1(func_mock)

        wrapper2 = mut.generator_driven(generator_function, *args2, **kwargs2)
        actual = wrapper2(func_mock)

        # verify
        # noinspection PyProtectedMember
        attr = getattr(actual, mut._GENERATOR_TEST_CASE_ATTRIBUTE)
        self.assertTrue(len(attr) == 2)

        # cleanup
        # noinspection PyProtectedMember
        delattr(func_mock, mut._GENERATOR_TEST_CASE_ATTRIBUTE)


EXPECTED_DATA = [((3, 6, 2), {EXPECTED_EXCEPTION: None, TEST_NAME: '6_div_2'}),
                 ((2, 4, 2), {EXPECTED_EXCEPTION: None, TEST_NAME: '4_div_2'}),
                 ((None, 1, 0), {EXPECTED_EXCEPTION: ZeroDivisionError, TEST_NAME: 'div_by_0'})]


# noinspection PyUnusedLocal
def example_generator_function(*args, **kwargs):
    """
    Example generator function that reads from a list.
    :param args: the argument list, it is ignored
    :param kwargs: the keyword argument list, it is ignored
    """
    for entry in EXPECTED_DATA:
        yield entry


class ExampleCSVFileParserTests(unittest.TestCase):
    def test_return_values(self):
        data = []

        for row in example_generator_function():
            data.append(row)
            # this is a list of tuples which correspond to the test_case_arguments

        self.assertTrue(data, EXPECTED_DATA)


def assertEqual(expected_result, actual, message=""):
    if not expected_result == actual:
        raise AssertionError(message)  # pragma: no cover


class Demo(unittest.TestCase):
    def test_decorator_test_case_read_data_from_file(self):
        # arrange
        args = ('a', 'b', 'b')
        kwargs = {'special': '????'}

        @mut.use_data_driven_decorators
        class MockTestCase(object):
            def __init__(self):
                self.passed_args = []

            @mut.generator_driven(example_generator_function, *args, **kwargs)
            def test_mocked_function(self, expected_result, number, divisor):
                self.passed_args.append((expected_result, number, divisor))
                result = number / divisor
                assertEqual(expected_result, number / divisor,
                            "expected {0} is not equal to actual {1}".format(expected_result, result))

        mock = MockTestCase()

        # act
        for m in [method for method in dir(mock) if method.startswith("test_") and callable(getattr(mock, method))]:
            method = getattr(mock, m)
            method()

        # verify
        expected = [(2, 4, 2), (3, 6, 2), (None, 1, 0)]
        self.assertEqual(mock.passed_args, expected)


if __name__ == '__main__':
    # unittest.main(verbosity=10)
    unittest.main()  # pragma: no cover
