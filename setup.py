#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
if hasattr(os, 'link'):
    del os.link  # as suggested here to avoid some hardlinking problems: http://stackoverflow.com/questions/17223047/why-is-my-python-setup-script-failing
import sys


try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

if sys.argv[-1] == 'publish':
    os.system('python setup.py sdist upload')
    sys.exit()

readme = open('README.rst').read()
history = open('HISTORY.rst').read().replace('.. :changelog:', '')

setup(
    name='datadriventestingdecorators',
    version='2.0.3.0',
    description='Data driven testing decorators.',
    long_description=readme + '\n\n' + history,
    author='DR0ID',
    author_email='dr0iddr0id [at] googlemail [dot] com',
    url='https://bitbucket.org/dr0id/data-driven-testing-decorators',
    py_modules = ['datadriventestingdecorators'],
    packages=[],
    package_dir={},
    include_package_data=True,
    install_requires=[
    ],
    license="BSD",
    zip_safe=False,
    keywords='data-driven-testing-decorators',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Software Development :: Testing',
    ],
    test_suite='tests',
)
